package com.example.blogapp

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    private lateinit var preferences: BlogPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        preferences = BlogPreferences(this)
        if(preferences.isLoggedIn()) {
            startMainActivity()
            finish()
            return
        }
        setContentView(R.layout.activity_login)

        loginButton.setOnClickListener({ onLoginClicked() })
        /**
         * 1. We need to retrieve editText property of TextInputLayout
         * 2. Add text change listener TextInputEditText#addTextChangedListener, this method requires an object of TextWatcher along with TextInputLayout
         * 3. Inside TextWatcher#onTextChanged method we need to implement our code to remove the error
         */
        textUsernameLayout.editText?.addTextChangedListener(createTextWatcher(textUsernameLayout, "Username must not be empty!"))
        textPasswordLayout.editText?.addTextChangedListener(createTextWatcher(textPasswordLayout, "Password must not be empty!"))
    }

    private fun onLoginClicked() {
        val username: String = textUsernameLayout.editText?.text.toString()
        val password: String = textPasswordLayout.editText?.text.toString()
        var flag = true
        if(username.isEmpty()) {
            textUsernameLayout.error = "Username must not be empty!"
            flag = false
        }
        if(password.isEmpty()) {
            textPasswordLayout.error = "Password must not be empty!"
            flag = false
        }
        if(flag){
            if(username != "studylink" || password != "a")
                showErrorDialog()
            else {
                performLogin()
            }
        }
    }

    private fun performLogin() {
        preferences.setLoggedIn(true)
        textUsernameLayout.isEnabled = false
        textPasswordLayout.isEnabled = false
        loginButton.visibility = View.INVISIBLE
        progressBar.visibility = View.VISIBLE

        Handler().postDelayed({
            startMainActivity()
            finish()
        }, 2000)
    }

    private fun startMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }


    private fun createTextWatcher(textInput: TextInputLayout, message: String): TextWatcher? {
        return object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // NOT USED
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(textInput.editText?.text.toString().length != 0){
                    textInput.error = null
                } else {
                    textInput.error = message
                }
            }

            override fun afterTextChanged(s: Editable?) {
                // NOT USED
            }
        }
    }
    private fun showErrorDialog() {
        AlertDialog.Builder(this)
                .setTitle("Login Failed")
                .setMessage("Username or Password is not correct. Please try again.")
                .setPositiveButton("Ok") { dialog, which -> dialog.dismiss() }
                .show()
    }
}